#!/usr/bin/env python3
# coding: utf-8


import argparse
from construct import *
import sys
from datetime import datetime


parser = argparse.ArgumentParser(description="Convert RM42 (UKK) binary log files to CSV format.")
parser.add_argument('input', metavar="INPUT", type=str, help='Input file (.dat format)')
parser.add_argument('output', metavar="OUTPUT", type=str, help='Output file (.csv format)')
parser.add_argument('-n', help="Store timestamp with a number", action='store_true')
parser.add_argument('--ms-84', help="Add milliseconds to each timestamp assuming that the data follows a frequency of 84Hz", action='store_true')

args = parser.parse_args()


def binToHexUKK(val):
    big = int(val / 16)
    little = val % 16
    return big * 10 + little

input_file = args.input.strip()
output_file= args.output.strip()
print("Input:", input_file)
print("Output:", output_file)


number = args.n
ms84 = args.ms_84

encoding = Int16sl


d_1sec = Struct(
            "aaaa" / Int16ub,
            "day" / Int8ub,
            "month" / Int8ub,
            "year" / Int8ub,
            "hour" / Int8ub,
            "minutes" / Int8ub,
            "seconds" / Int8ub,
            "entries" / Array(84, Array(3, encoding)))


step_ms = int(1000. / 84)
step_us = step_ms * 1000


with open(input_file, 'rb') as f_input:
    with open(output_file, 'w') as f_output:
        print("timestamp;X_ax;Y_ax;Z_ax", file=f_output)
        while f_input:
            try:
                result = d_1sec.parse_stream(f_input)
                year = 2000 + binToHexUKK(result.year)
                month = binToHexUKK(result.month)
                day = binToHexUKK(result.day)
                hour = binToHexUKK(result.hour)
                minutes = binToHexUKK(result.minutes)
                seconds = binToHexUKK(result.seconds)
                if (year != 2000 or month != 0 or day != 0 or hour != 0 or minutes != 0 or seconds != 0) and month <= 12 and month >= 1:
                    
                    for i, entry in enumerate(result.entries):
                        entry_csv = ';'.join([str(e) for e in entry])
                        if number:
                            nb = datetime(year, month, day, hour, minutes, seconds, i * step_us if ms84 else 0).timestamp()
                            print('{:f};{:s}'.format(nb, entry_csv), file=f_output)
                        else:
                            if ms84:
                                print('{:02d}/{:02d}/{:04d} {:02d}:{:02d}:{:02d}.{:03d};{:s}'.format(day, month, year, hour, minutes, seconds, i * step_ms, entry_csv), file=f_output)
                            else:
                                print('{:02d}/{:02d}/{:04d} {:02d}:{:02d}:{:02d};{:s}'.format(day, month, year, hour, minutes, seconds, entry_csv), file=f_output)
                    
                else:
                    print("skip wrong entry")
                
            except:
                print("Skip end of file")
                break
