#!/usr/bin/env python3
# coding: utf-8


import argparse
from construct import *
import sys
from datetime import datetime

import numpy as np
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description="Convert RM42 (UKK) binary log files to CSV format.")
parser.add_argument('input', metavar="INPUT", type=str, help='Input file (.dat format)')

args = parser.parse_args()


def binToHexUKK(val):
    big = int(val / 16)
    little = val % 16
    return big * 10 + little

def add_diff(histo, diff):
    for i, d in enumerate(diff):
        histo[i].append(d)

input_file = args.input.strip()
print("Input:", input_file)


encoding = Int16sl


d_1sec = Struct(
            "aaaa" / Int16ub,
            "day" / Int8ub,
            "month" / Int8ub,
            "year" / Int8ub,
            "hour" / Int8ub,
            "minutes" / Int8ub,
            "seconds" / Int8ub,
            "entries" / Array(84, Array(3, encoding)))


diff_inside = [ [], [], [] ]
diff_consecutive = [ [], [], [] ]
last_timestamp = -1
last_values = []

density = []
firstHour = -1

with open(input_file, 'rb') as f_input:
    while f_input:
        try:
            result = d_1sec.parse_stream(f_input)
            year = 2000 + binToHexUKK(result.year)
            month = binToHexUKK(result.month)
            day = binToHexUKK(result.day)
            hour = binToHexUKK(result.hour)
            minutes = binToHexUKK(result.minutes)
            seconds = binToHexUKK(result.seconds)
            if (year != 2000 or month != 0 or day != 0 or hour != 0 or minutes != 0 or seconds != 0) and month <= 12 and month >= 1:                    
                nb = int(datetime(year, month, day, hour, minutes, seconds, 0).timestamp())
                
                not_zero = False

                # for diff
                for i, entry in enumerate(result.entries):
                    if i == 0:
                        if last_timestamp == nb - 1:
                            diff = [ x - y for (x, y) in zip(entry, last_values)]
                            add_diff(diff_consecutive, diff)
                    else:
                        diff = [ x - y for (x, y) in zip(entry, last_values)]
                        add_diff(diff_inside, diff)
                
                    last_values = entry
                    if entry[0] != 0 or  entry[1] != 0 or entry[2]:
                        not_zero = True
                last_timestamp = nb
                
                # for density
                h = int(nb / 60 / 60)
                if firstHour == -1:
                    firstHour = h
                shift_h = h - firstHour
                if len(density) <= shift_h:
                    while len(density) <= shift_h:
                        density.append(0)
                if not_zero:
                    density[shift_h] += 1
                
            else:
                print("skip wrong entry")

        except:
            print("Skip end of file")
            break


print("Diff between two consecutive values")
for i, diffs in enumerate(diff_inside):
    print("inner[", i, "]: STD", np.std(diffs), "MEAN", np.mean(diffs))

print("Diff between last element of a 84 values serie and the first alement of the next serie")
for i, diffs in enumerate(diff_consecutive):
    print("inner[", i, "]: STD", np.std(diffs), "MEAN", np.mean(diffs))

print("Density per hour (expected: " + str(60 * 60) + " recordings)")
print(density)
