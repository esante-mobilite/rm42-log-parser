#!/usr/bin/env python3
# coding: utf-8


import argparse
import math
from construct import *
import sys
from datetime import datetime


parser = argparse.ArgumentParser(description="Convert RM42 (UKK) binary log files to CSV format.")
parser.add_argument('input', metavar="INPUT", type=str, help='Input file (.dat format)')

args = parser.parse_args()

debug = False

def binToHexUKK(val):
    big = int(val / 16)
    little = val % 16
    return big * 10 + little

input_file = args.input.strip()
print("Input:", input_file)

block_size = 84

encoding = Int16sl
d_1sec = Struct(
            "aaaa" / Int16ub,
            "day" / Int8ub,
            "month" / Int8ub,
            "year" / Int8ub,
            "hour" / Int8ub,
            "minutes" / Int8ub,
            "seconds" / Int8ub,
            "entries" / Array(block_size, Array(3, encoding)))



class Block:
    invalid_year = 4000
    def __init__(self, year, month, day, hour, minutes, seconds):
        self.year = year
        self.month = month
        self.day = day
        self.hour = hour
        self.minutes = minutes
        self.seconds = seconds
        self.valid = self.year != Block.invalid_year 
        self.datetime = datetime(self.year, self.month, self.day, self.hour, self.minutes, self.seconds, 0)
        self.records = []
        
    def fake_block():
        return Block(Block.invalid_year, 1, 1, 0, 0, 0)
    
    def is_valid(self):
        return self.valid
    
    def add_record(self, record):
        self.records.append(record)

    def get_timestamp(self):
        return int(self.datetime.timestamp())

class Batch:
    def __init__(self, block, spacing):
        self.blocks = [block]
        self.local_timestamp = 0
        self.max_shift = 1.0
        self.required_shift = 0.0
        self.spacing = spacing
        
    def concatenate_if_possible(self, block):
        tSecBlock = block.get_timestamp()
        tSecLastBlock = self.blocks[len(self.blocks) - 1].get_timestamp()
        if not( tSecBlock == tSecLastBlock or tSecBlock == tSecLastBlock + 1):
            # is not concatenable (existing gap)
            return False
        
        next_local_timestamp = self.local_timestamp + block_size + self.spacing
        deltaS = self.get_delta_from_first(block)
        
        if math.floor(next_local_timestamp / 100) < deltaS:
            required_shift = deltaS - next_local_timestamp / 100
            if required_shift < self.max_shift:
                # is concatenable with shift
                # update minimum required shift
                if required_shift > self.required_shift:
                    self.required_shift = required_shift
                # add element
                self.local_timestamp = next_local_timestamp
                self.blocks.append(block)
                return True
            else:
                # is not concatenable due to required shift incompatible with precomputed maximum shift
                return False
        else:
            # is concatenable without shift
            # compute max shift
            m = next_local_timestamp/100 + 1 - deltaS
            if self.max_shift > m:
                self.max_shift = m
            # add element
            self.local_timestamp = next_local_timestamp
            self.blocks.append(block)
            return True
        
    def get_first_datetime(self):
        return self.blocks[0].datetime.timestamp()
        
    def get_delta_from_first(self, block):
        return block.datetime.timestamp() - self.get_first_datetime()


    def get_first_datetime_with_shift(self):
        return self.get_first_datetime() + self.required_shift
    
    def get_spacing(self):
        return self.spacing

class TimeSerie:
    
    def __init__(self):
        self.batches = []
        self.default_spacing = 4
        
    def add_block(self, block):
        if len(self.batches) == 0 or not self.batches[len(self.batches) - 1].concatenate_if_possible(block):
                self.batches.append(Batch(block, self.default_spacing))
                
    def to_array(self):
        result = []
        for b in self.batches:
            i = 0
            for bb in b.blocks:
                if bb.is_valid():
                    for e in bb.records:
                        result.append([b.get_first_datetime_with_shift() + i / 100] + e)
                        if debug and len(result) > 1:
                            if result[len(result) - 1][0] <= result[len(result) - 2][0]:
                                print("~ Error in lines", result[len(result) - 1], "<", result[len(result) - 2])
                            elif abs(result[len(result) - 1][0] - result[len(result) - 2][0] - 0.01) > 0.0001:
                                print("~ Small gap", (result[len(result) - 1][0] - result[len(result) - 2][0] - 0.01), result[len(result) - 1], "<->", result[len(result) - 2])
                        i += 1
                    i += b.get_spacing()
        return result
    
    def to_csv(self):
        array = self.to_array()
        result = "\n".join(['{:f};{:s}'.format(l[0], ";".join([str(v) for v in l[1:]])) for l in array])
        return result

    
    def is_empty(self):
        return len(self.batches) == 0

ts = TimeSerie()

with open(input_file, 'rb') as f_input:
    while f_input:
        try:
            result = d_1sec.parse_stream(f_input)
            
            if result.aaaa == 43690:
                year = 2000 + binToHexUKK(result.year)
                month = binToHexUKK(result.month)
                day = binToHexUKK(result.day)
                hour = binToHexUKK(result.hour)
                minutes = binToHexUKK(result.minutes)
                seconds = binToHexUKK(result.seconds)
                b = Block(year, month, day, hour, minutes, seconds)
            else:
                if ts.is_empty():
                    continue
                else:
                    # fake timestamp
                    b = Block.fake_block()
                
            for entry in result.entries:
                b.add_record(entry)
        
            ts.add_block(b)
            
        except StreamError as e:
            if debug:
                print("Skip end of file")
            break
        except:
            print("Unknown error", sys.exc_info()[0])
            break


csv = ts.to_csv()

print(csv)
